<?php

use Illuminate\Database\Seeder;

class BusinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Businesses')->insert([
    		'name' => 'Import',
    	]);
    	DB::table('Businesses')->insert([
    		'name' => 'Export',
    	]);
    }
}
