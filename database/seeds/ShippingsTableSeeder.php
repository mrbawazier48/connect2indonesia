<?php

use Illuminate\Database\Seeder;

class ShippingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Shippings')->insert([
    		'name' => 'SEA',
    		'price' => '200000',
    	]);
    	DB::table('Shippings')->insert([
    		'name' => 'SEA CARGO',
    		'price' => '350000',
    	]);
    	DB::table('Shippings')->insert([
    		'name' => 'AIR',
    		'price' => '300000',
    	]);
    	DB::table('Shippings')->insert([
    		'name' => 'AIR CARGO',
    		'price' => '550000',
    	]);
    }
}
