<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('Cities')->insert([ 	
    		'name' => 'Jakarta',
    		'Countries_id' => '1',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Batam',
    		'Countries_id' => '1',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Kuala Lumpur',
    		'Countries_id' => '2',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Perak',
    		'Countries_id' => '2',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Pulau Ujong',
    		'Countries_id' => '3',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Pulau Ubin',
    		'Countries_id' => '3',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Bangkok',
    		'Countries_id' => '4',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Phangnga',
    		'Countries_id' => '4',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Manila',
    		'Countries_id' => '5',
    	]);
    	DB::table('Cities')->insert([
    		'name' => 'Iloilo City',
    		'Countries_id' => '5',
    	]);
    }
}
