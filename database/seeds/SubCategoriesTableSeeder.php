<?php

use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Sub_Categories')->insert([
    		'name' => 'Kopi',
    		'categories_id' => '1',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Teh',
    		'categories_id' => '1',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Wol',
    		'categories_id' => '2',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Sapi',
    		'categories_id' => '2',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Kayu',
    		'categories_id' => '3',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Karet',
    		'categories_id' => '3',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Besi',
    		'categories_id' => '4',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Pakaian',
    		'categories_id' => '4',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Minyak Bumi',
    		'categories_id' => '5',
    	]);
    	DB::table('Sub_Categories')->insert([
    		'name' => 'Emas',
    		'categories_id' => '5',
    	]);
    }
}
