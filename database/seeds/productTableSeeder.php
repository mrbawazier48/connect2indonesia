<?php

use Illuminate\Database\Seeder;

class productTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Susu Campur',
            'description' => 'Susu Vanila Campur Coklat',
            'avatar' => '',
            'price'=> 10000,
            'Categories_id' => 1,
            'Trades_id' => 2,
            'Companies_id' => 1
        ]);
    }
}
