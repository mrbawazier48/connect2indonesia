<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Andra',
            'email' => 'andra@mail',
            'password' => Hash::make('123123123123')
        ]);
    }
}
