<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('Companies')->insert([
            'name' => 'Indo Raya',
            'description' => 'Perusahaan Material',
            'address' => 'Jl. Material',
            'web'=> 'www.indoraya.com',
            'telephone' => 791564,
            'avatar' => "",
            'user_id' =>1,
            "Cities_id" =>1
        ]);

//        $companies = \App\Models\Company::all();
//
//        \App\Models\City::all()->each(function ($city) use ($companies) {
//            $city->companies()->attach(
//                $companies->random(rand(1,$companies->count()))->pluck('id')->toArray()
//            );
//        });
    }
}
