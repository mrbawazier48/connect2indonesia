<?php

use Illuminate\Database\Seeder;

class Delivery_ChargeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('Delivery_Charge')->insert([
    		'name' => 'IND - SGN',
    		'price' => '1000000',
    	]);
    	DB::table('Delivery_Charge')->insert([
    		'name' => 'IND - MYS',
    		'price' => '1200000',
    	]);
    	DB::table('Delivery_Charge')->insert([
    		'name' => 'IND - THA',
    		'price' => '1500000',
    	]);
    	DB::table('Delivery_Charge')->insert([
    		'name' => 'IND - PHL',
    		'price' => '1600000',
    	]);
    }
}
