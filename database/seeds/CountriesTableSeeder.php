<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Countries')->insert([
    		'name' => 'Indonesia',
    	]);
    	DB::table('Countries')->insert([
    		'name' => 'Malaysia',
    	]);
    	DB::table('Countries')->insert([
    		'name' => 'Singapore',
    	]);
    	DB::table('Countries')->insert([
    		'name' => 'Thailand',
    	]);
    	DB::table('Countries')->insert([
    		'name' => 'Philippines',
    	]);
    }
}
