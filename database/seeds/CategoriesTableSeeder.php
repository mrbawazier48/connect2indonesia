<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Categories')->insert([
    		'name' => 'Pertanian',
    	]);
    	DB::table('Categories')->insert([
    		'name' => 'Peternakan',
    	]);
    	DB::table('Categories')->insert([
    		'name' => 'Kehutanan',
    	]);
    	DB::table('Categories')->insert([
    		'name' => 'Industri & Pengolahan',
    	]);
    	DB::table('Categories')->insert([
    		'name' => 'Tambang',
    	]);
    }
}
