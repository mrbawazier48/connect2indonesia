<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            BusinessesTableSeeder::class,
        	CategoriesTableSeeder::class,
        	CountriesTableSeeder::class,
        	CitiesTableSeeder::class,
            CompanyTableSeeder::class,
            Delivery_ChargeTableSeeder::class,
        	ShippingsTableSeeder::class,
        	SubCategoriesTableSeeder::class,
        	TradesTableSeeder::class,
            productTableSeeder::class,
        ]);
    }
}
