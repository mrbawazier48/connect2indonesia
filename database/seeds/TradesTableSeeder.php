<?php

use Illuminate\Database\Seeder;

class TradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Trades')->insert([
    		'name' => 'Import',
    	]);
    	DB::table('Trades')->insert([
    		'name' => 'Export',
    	]);
    }
}
