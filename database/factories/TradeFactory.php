<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Trade;
use Faker\Generator as Faker;

$factory->define(Trade::class, function (Faker $faker) {

    return [
        'name' => $faker->word
    ];
});
