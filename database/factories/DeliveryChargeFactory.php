<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryCharge;
use Faker\Generator as Faker;

$factory->define(DeliveryCharge::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'price' => $faker->word
    ];
});
