<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->word,
        'avatar' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'Categories_id' => $faker->randomDigitNotNull,
        'Trades_id' => $faker->randomDigitNotNull,
        'Companies_id' => $faker->randomDigitNotNull
    ];
});
