<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Invoices;
use Faker\Generator as Faker;

$factory->define(Invoices::class, function (Faker $faker) {

    return [
        'date' => $faker->word,
        'Buyer' => $faker->randomDigitNotNull,
        'Seller' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
