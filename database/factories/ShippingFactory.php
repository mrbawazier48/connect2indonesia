<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Shipping;
use Faker\Generator as Faker;

$factory->define(Shipping::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'price' => $faker->randomDigitNotNull
    ];
});
