<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SubCategories;
use Faker\Generator as Faker;

$factory->define(SubCategories::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'Categories_id' => $faker->randomDigitNotNull
    ];
});
