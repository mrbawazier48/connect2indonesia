<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\InvoiceItem;
use Faker\Generator as Faker;

$factory->define(InvoiceItem::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'total' => $faker->randomDigitNotNull,
        'Invoices_id' => $faker->randomDigitNotNull,
        'Shippings_id' => $faker->randomDigitNotNull,
        'Delivery_Charge_id' => $faker->randomDigitNotNull
    ];
});
