<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->word,
        'address' => $faker->word,
        'web' => $faker->word,
        'telephone' => $faker->randomDigitNotNull,
        'avatar' => $faker->word,
        'Cities_id' => $faker->randomDigitNotNull
    ];
});
