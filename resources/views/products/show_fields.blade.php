<div class="table-responsive">
    <table class="table table-vcenter">
        <tbody>
        <tr>
            <td style="width: 8%;">Name</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->name!!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Description</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->description !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Avatar</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->avatar !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Price</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->price !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Categories</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->categories->name !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Trades</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $product->trades->name !!}</td>
        </tr>
        </tbody>
    </table>
</div>

<!-- Name Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('name', 'Name:') !!}--}}
{{--    <p>{!! $product->name !!}</p>--}}
{{--</div>--}}

<!-- Description Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('description', 'Description:') !!}--}}
{{--    <p>{!! $product->description !!}</p>--}}
{{--</div>--}}

<!-- Avatar Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('avatar', 'Avatar:') !!}--}}
{{--    <p>{!! $product->avatar !!}</p>--}}
{{--</div>--}}

<!-- Price Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('price', 'Price:') !!}--}}
{{--    <p>{!! $product->price !!}</p>--}}
{{--</div>--}}

<!-- Categories Id Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('Categories_id', 'Categories Id:') !!}--}}
{{--    <p>{!! $product->categories->name !!}</p>--}}
{{--</div>--}}

<!-- Trades Id Field -->
{{--<div class="form-group">--}}
{{--    {!! Form::label('Trades_id', 'Trades Id:') !!}--}}
{{--    <p>{!! $product->trades->name !!}</p>--}}
{{--</div>--}}

