<div class="table-responsive">
    <table class="table" id="products-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Avatar</th>
            <th>Price</th>
            <th>Categories</th>
            <th>Trades Type</th>
            <th>Milik Perusahaan</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{!! $product->name !!}</td>
                <td>{!! $product->description !!}</td>
                <td>{!! $product->avatar !!}</td>
                <td>{!! $product->price !!}</td>
                <td>{!! $product->categories->name !!}</td>
                <td>{!! $product->trades->name !!}</td>
                <td>{{  $product->companies->name }}</td>
                <td class="row">
                    {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('products.show', [$product->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        @if (Auth::user()->id==1)
                            <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-success btn-xs'
                               title="Edit"><i class="si si-pencil"></i></a>
                        @else
                            <a href="{!! route('products.editProductCompany', [$company_id,$product->id]) !!}" class='btn btn-success btn-xs'
                               title="Edit"><i class="si si-pencil"></i></a>
                        @endif

                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-auto btn-group">
                        <a href="{{ route('products.addToCart',[$product->id]) }}" class="btn btn-success btn-xs"
                           title="Add to Cart"><i class="fa fa-cart-plus"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
