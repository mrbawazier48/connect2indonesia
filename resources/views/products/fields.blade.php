<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Avatar Field -->
<div class="form-group col-sm-12">
    {!! Form::label('avatar', 'Avatar:') !!}
    {!! Form::file('avatar', ['class' => 'form-control-file']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Categories Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Categories', 'Categories:') !!}
    {!! Form::select('Categories_id', $categories, null, ['class' => 'form-control']) !!}

</div>

<!-- Trades Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Trades_id', 'Trades Type:') !!}
    {!! Form::select('Trades_id', $trades, null, ['class' => 'form-control']) !!}
</div>

<!-- Trades Id Field -->
<div class="form-group col-sm-12">
    @if(Auth::user()->id==1)
        {!! Form::label('Companies_id', 'Companies Name :') !!}
        {!! Form::select('Companies_id', $companies, null, ['class' => 'form-control']) !!}
    @else
        {!! Form::hidden('Companies_id', $company_id, ['id' => 'Companies_id']) !!}
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>
