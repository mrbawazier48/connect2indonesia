@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1 class="block-title">Cart</h1>
        </div>
        <div class="block-content">
            <p>
                @include('flash::message')
            </p>
            @if (Session::has('cart'))
                @include('products.item')
            @else
                <h1>No Item </h1>
            @endif
        </div>
    </div>
@endsection

