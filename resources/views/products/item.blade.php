 <div class="table-responsive">
    @foreach($companies as $company)
        <table class="table" id="products-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Increase/Decrease</th>
                <th>Sub Total</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {{--            {{dd($company)}}--}}
            @foreach($company["products"] as $product)
                {{--                        {{dd($product)}}--}}
                <tr>
                    <td>{!! $product['product']->name !!}</td>
                    <td align="left">Rp. {{  "\t".number_format($product['product']->price,2,',','.') }}</td>
                    <td class="row">
                        {{--                    {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}--}}
                        <div class='col-auto btn-group'>
                            <a href="{{ route('decrease.item', $product['product']->id) }}"
                               class='btn btn-success btn-xs'
                               title="Edit"><i class="fa fa-minus"></i></a>
                            <a href="#" class="btn btn-info">{!! $product['subTotalQuantity'] !!}</a>
                            <a href="{{ route('increase.item', $product['product']->id) }}"
                               class='btn btn-danger btn-xs'
                               title="Detail"><i class="fa fa-plus"></i></a>
                        </div>
                    </td>
                    <td>Rp. {!! number_format($product['subTotalPrice'],2,',','.') !!}</td>
                    <td>
                        <a href="{{ route("main.checkout",[$product['product']->id]) }}" class="btn btn-success">Checkout</a>
                    </td>
                </tr>

            @endforeach
            <tr>
                <td>Cart Total Price :</td>
                <td></td>
                <td></td>
                <td>Rp. {{ number_format($company["totalPrice"],2,',','.') }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td align="">
                </td>
            </tr>
            </tbody>
        </table>
    @endforeach
</div>
