@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1 class="block-title">Products</h1>
            <div class="block-option">
                <button type="button" class="btn btn-secondary">
                    @if (Auth::user()->id==1)
                        <a href="{!! route('products.create') !!}">Add New</a>
                    @else
                        <a href="{!! route('products.createProductCompany',[$company_id]) !!}">Add New</a>
                    @endif
                </button>
            </div>
        </div>
        <div class="block-content">
            <p>
                @include('flash::message')
            </p>
            @include('products.table')
        </div>
    </div>
@endsection

