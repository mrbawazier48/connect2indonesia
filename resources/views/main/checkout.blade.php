@extends('layouts.app_codebase')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="block block-fx-shadow">
                    <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#crypto-buy">Checkout</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="crypto-buy">
                            <form action="{{ route("invoices.store") }}" method="post">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-4" for="crypto-buy-to">Delivery Location</label>
                                    <div class="col-4">
                                        {!! Form::select('delivery', $deliveries , ["walla"=>"Select One"] , ['class' => 'form-control',"onchange"=>"deliveryPicker(this.value)"]) !!}
                                    </div>
                                    <div class="col-4">
                                        {!! Form::select('delivery', $deliveryPrices , ["walla"=>"Select One"] , ['class' => 'form-control',"disabled"=>"","style"=>"-webkit-appearance: none;","id"=>"deliveryPrices" ]) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4" for="crypto-buy-to">Shipping Method</label>
                                    <div class="col-4">
                                        {!! Form::select('shipping', $shippings , null , ['class' => 'form-control',"onchange"=>"shippingPicker(this.value)"]) !!}
                                    </div>
                                    <div class="col-4">
                                        {!! Form::select('shipping', $shippingPrices , null , ['class' => 'form-control',"disabled"=>"","style"=>"-webkit-appearance: none;","id"=>"shippingPrices"]) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12" for="crypto-buy-from">Buy From</label>
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="table" id="products-table">
                                                <thead>
                                                <tr>
                                                    <th>Detail</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        {{$cartItem["products"][$product->id]["product"]->name}}
                                                    </td>
                                                    <td>
                                                        {{ $cartItem["products"][$product->id]["subTotalQuantity"] }}
                                                    </td>
                                                    <td id="productPrice">
                                                        {{ $cartItem["products"][$product->id]["subTotalPrice"] }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="deliveryLocationName">
                                                        Delivery Service
                                                    </td>
                                                    <td id="deliveryLocationQuantity">
                                                    </td>
                                                    <td id="deliveryLocationPrice">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="shippingName">
                                                        Shipping Service
                                                    </td>
                                                    <td id="shippingQuantity">
                                                    </td>
                                                    <td id="shippingPrice">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Total</td>
                                                    <td></td>
                                                    <td>
                                                        <span id="total">
                                                            {{ $cartItem["products"][$product->id]["subTotalPrice"] }}
                                                        </span>
                                                        <input type="hidden" id="totalPrice" name="total" value="">
                                                        <input type="hidden" name="Buyer" value="{{ Auth::user()->id }}">
                                                        <input type="hidden" name="Seller" value="{{ $product->Companies_id }}">
                                                        <input type="hidden" name="product" value="{{ $cartItem["products"][$product->id]["product"]->id }}">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-hero btn-lg btn-block btn-alt-primary">
                                            Checkout
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('javascript')
    <script type="text/javascript">
        var praductPrice = document.getElementById("totalProduct").innerHTML;
        document.getElementById("total").innerHTML = productPrice;

        function deliveryPicker(id) {
            document.getElementById("deliveryPrices").selectedIndex = id;
            var selected = document.getElementById("deliveryPrices");
            var price = selected.options[selected.selectedIndex].text;
            document.getElementById("deliveryLocationPrice").innerHTML = price;
            trans(price);
        };

        function shippingPicker(id){
            document.getElementById("shippingPrices").selectedIndex = id;
            var selected = document.getElementById("shippingPrices");
            var price = selected.options[selected.selectedIndex].text;
            document.getElementById("shippingPrice").innerHTML = price;
            trans(price);
        };
        
        function trans(price) {
            var total = document.getElementById("total").innerHTML;
            total = parseInt(total);
            price = parseInt(price);
            total = total + price;
            document.getElementById("total").innerHTML = total.toString();
            document.getElementById("totalPrice").value = total;
            
        }
    </script>
@endsection