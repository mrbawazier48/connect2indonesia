@extends('layouts.app_codebase')

@section('content')
    <div class="block block-rounded block-transparent bg-image bg-image-bottom"
         style="background-image: url('{{ asset('/media/photos/photo13@2x.jpg') }}');">
        <div class="block-content bg-white-op">
            <div class="py-20 text-center">
                <h1 class="font-w700 mb-10">Many option to Choose</h1>
                {{--                <h2 class="h4 font-w400 text-muted">Hey Admin, create something amazing!</h2>--}}
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Dummy content -->
    {{--    <div class="card-deck">--}}
    {{--        @foreach($products as $product)--}}
    {{--            <div class="card" style="width:400px">--}}
    {{--                <img src="{{ asset('uploads/product/'.$product->avatar) }}" class="card-img-top border-black-op-b"--}}
    {{--                     alt="...">--}}
    {{--                <div class="card-body">--}}
    {{--                    <h5 class="card-title">{{ $product->name }}</h5>--}}
    {{--                    <p class="card-text">{{ $product->description }}</p>--}}
    {{--                    <a href="{{ Auth::user()?route('products.addToCart',[$product->id]):url('/login') }}"--}}
    {{--                       class="btn btn-success btn-xs"--}}
    {{--                       title="Add to Cart"><i class="fa fa-cart-plus"></i></a>--}}
    {{--                    <a href="#" class="btn btn-outline-secondary" disabled="">{{ 'Rp. '.$product->price }}</a>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        @endforeach--}}
    {{--    </div>--}}
    <div class="row">
        @foreach ($products as $product)
            <div class="col-md-6 col-xl-4">
                <!-- Property -->
                <div class="block block-rounded">
                    <div class="block-content p-0 overflow-hidden">
                        <a class="img-link" href="{{ route("main.detail",$product->name) }}">
                            <img class="img-fluid rounded-top" src="{{ asset('uploads/product/'.$product->avatar) }}"
                                 alt="">
                        </a>
                    </div>
                    <div class="block-content border-bottom">
                        <h4 class="font-size-h5 mb-10">{{ $product->name }}</h4>
                        <h5 class="font-size-h1 font-w300 mb-5">Rp. {{ number_format($product->price,2,",",".") }}</h5>
                        <p class="text-muted">
                            <i class="fa fa-building"></i> {{ $product->companies->name }}
                        </p>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-12">
                                <a href="{{ Auth::user()?route('products.addToCart',[$product->id]):url('/login') }}"
                                                       class="btn btn-success btn-sm btn-hero btn-noborder btn-block"
                                                       title="Add to Cart"><i class="fa fa-cart-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Property -->
            </div>
        @endforeach
    </div>
    <!-- END Dummy content -->
@endsection
