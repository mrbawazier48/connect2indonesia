@extends('layouts.app_codebase')

@section('content')
    <div class="content">
        <div class="my-50 text-center">
            <h2 class="font-w700 text-black mb-10">
                {{ $product->name }}
            </h2>
            <h3 class="h5 text-muted mb-0">
                <i class="fa fa-building mr-5"></i> {{ $product->companies->name }}
            </h3>
        </div>
        <div class="block block-rounded block-fx-shadow">
            <div class="block-content p-0 bg-image" style="background-image: url('{{ asset('uploads/product/'.$product->avatar) }}');">
                <div class="px-20 py-150 bg-black-op text-center rounded-top">
                    <h5 class="font-size-h1 font-w300 text-white mb-10">Rp. {{ number_format($product->price,2,",",".") }}</h5>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-md-12 order-md-1 py-20">
                        <p>{{$product->description  }}</p>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full border-top clearfix">
                <a class="btn btn-hero btn-alt-success float-right" href="{{ Auth::user()?route('products.addToCart',[$product->id]):url('/login') }}">
                    <i class="fa fa-cart-plus"></i>
                </a>
                <a class="btn btn-hero btn-alt-primary" href="{!! url()->previous() !!}">
                    <i class="fa fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
    </div>
@endsection