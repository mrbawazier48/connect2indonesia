@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1 class="block-title">Products</h1>
            <div class="block-option">
                <button type="button" class="btn btn-secondary"><a href="{!! route('categories.create') !!}">Add New</a>
                </button>
            </div>
        </div>
        <div class="block-content">
            <p>
                @include('flash::message')
            </p>
            @include('categories.table')
        </div>
    </div>

@endsection

