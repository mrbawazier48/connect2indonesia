<div class="table-responsive">
    <table class="table" id="categories-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{!! $category->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
