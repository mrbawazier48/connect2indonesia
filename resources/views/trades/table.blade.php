<div class="table-responsive">
    <table class="table" id="trades-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($trades as $trade)
            <tr>
                <td>{!! $trade->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['trades.destroy', $trade->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('trades.show', [$trade->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('trades.edit', [$trade->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
