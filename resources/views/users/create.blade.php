@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1>
                User
            </h1>
        </div>
        <div class="block-content">
            <p>
        @include('adminlte-templates::common.errors')
            </p>
                    {!! Form::open(['route' => 'users.store']) !!}

                        @include('users.fields')

                    {!! Form::close() !!}
                </div>
            </div>
@endsection
