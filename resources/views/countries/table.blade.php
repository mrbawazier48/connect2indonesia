<div class="table-responsive">
    <table class="table" id="countries-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($countries as $country)
            <tr>
                <td>{!! $country->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['countries.destroy', $country->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('countries.show', [$country->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('countries.edit', [$country->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
