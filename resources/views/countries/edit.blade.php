@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1>
                Country
            </h1>
        </div>
        <div class="block-content">
            <p>
                @include('adminlte-templates::common.errors')
            </p>
            {!! Form::model($country, ['route' => ['countries.update', $country->id], 'method' => 'patch']) !!}

            @include('countries.fields')

            {!! Form::close() !!}
        </div>
    </div>

@endsection
