@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1>
                Company
            </h1>
        </div>
        <div class="block-content">
            <p>
                @include('adminlte-templates::common.errors')
            </p>
            {!! Form::model($business, ['route' => ['businesses.update', $business->id], 'method' => 'patch']) !!}

            @include('businesses.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
