@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1>
            Companies
            </h1>
        </div>
        <div class="block-content">
            <p>
                @include('adminlte-templates::common.errors')
            </p>
            {!! Form::open(['route' => 'companies.store','files'=> true]) !!}

            @include('companies.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
