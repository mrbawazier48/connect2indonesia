<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Web Field -->
<div class="form-group col-sm-12">
    {!! Form::label('web', 'Web:') !!}
    {!! Form::text('web', null, ['class' => 'form-control']) !!}
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-12">
    {!! Form::label('telephone', 'Telephone:') !!}
    {!! Form::number('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Avatar Field -->
<div class="form-group col-sm-12">
    {!! Form::label('avatar', 'Avatar:') !!}
    {!! Form::file('avatar', ['class' => 'form-control-file']) !!}
</div>

<!-- Cities Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Cities_id', 'Cities:') !!}
    {!! Form::select('Cities_id', $cities, null, ['class' => 'form-control']) !!}
</div>

<!-- Cities Id Field -->
<div class="form-group col-sm-12">
    @if (Auth::user()->id==1)
        {!! Form::label('user_id', 'Admin:') !!}
        {!! Form::select('user_id', $users, null, ['class' => 'form-control']) !!}
    @else
        {!! Form::hidden('user_id', Auth::user()->id, ['id' => 'user_id']) !!}
    @endif
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('companies.index') !!}" class="btn btn-default">Cancel</a>
</div>
