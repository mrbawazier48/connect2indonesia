<div class="table-responsive">
    <table class="table table-vcenter">
        <tbody>
        <tr>
            <td style="width: 8%;">Name</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->name!!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Description</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->description !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Address</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->address !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Web</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->web !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Telephone</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->telephone !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Avatar</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->avatar !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">City</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $company->Cities->name !!}</td>
        </tr>
        </tbody>
    </table>
</div>

{{--<!-- Name Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('name', 'Name:') !!}--}}
{{--    <p>{!! $company->name !!}</p>--}}
{{--</div>--}}

{{--<!-- Description Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('description', 'Description:') !!}--}}
{{--    <p>{!! $company->description !!}</p>--}}
{{--</div>--}}

{{--<!-- Address Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('address', 'Address:') !!}--}}
{{--    <p>{!! $company->address !!}</p>--}}
{{--</div>--}}

{{--<!-- Web Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('web', 'Web:') !!}--}}
{{--    <p>{!! $company->web !!}</p>--}}
{{--</div>--}}

{{--<!-- Telephone Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('telephone', 'Telephone:') !!}--}}
{{--    <p>{!! $company->telephone !!}</p>--}}
{{--</div>--}}

{{--<!-- Avatar Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('avatar', 'Avatar:') !!}--}}
{{--    <p>{!! $company->avatar !!}</p>--}}
{{--</div>--}}

{{--<!-- Cities Id Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('Cities_id', 'Cities Id:') !!}--}}
{{--    <p>{!! $company->Cities_id !!}</p>--}}
{{--</div>--}}


