<div class="table-responsive">
    <table class="table" id="companies-table">
        <thead>
        <tr>
            <th>Name</th>
{{--            <th>Description</th>--}}
            <th>Address</th>
            <th>Web</th>
            <th>Telephone</th>
{{--            <th>Avatar</th>--}}
            <th>Cities Id</th>
            @if (Auth::user()->id==1)
                <th>Admin Name</th>
            @endif
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <tr>
                <td>{!! $company->name !!}</td>
{{--                <td>{!! $company->description !!}</td>--}}
                <td>{!! $company->address !!}</td>
                <td>{!! $company->web !!}</td>
                <td>{!! $company->telephone !!}</td>
{{--                <td>{!! $company->avatar !!}</td>--}}
                <td>{!! $company->cities->name !!}</td>
                @if (Auth::user()->id==1)
                <td>{{  $company->user->name }}</td>
                @endif
                <td>
                    {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('companies.show', [$company->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('companies.edit', [$company->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-auto btn-group">
                        <a href="{{ route('products.showProductCompany',[$company->id])}}" class="btn btn-warning btn-xs" title="Product List"><i class="fa fa-product-hunt"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
