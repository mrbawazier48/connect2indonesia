

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{!! route('products.index') !!}"><i class="fa fa-edit"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('companies*') ? 'active' : '' }}">
    <a href="{!! route('companies.index') !!}"><i class="fa fa-edit"></i><span>Companies</span></a>
</li>

<li class="{{ Request::is('businesses*') ? 'active' : '' }}">
    <a href="{!! route('businesses.index') !!}"><i class="fa fa-edit"></i><span>Businesses</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{!! route('cities.index') !!}"><i class="fa fa-edit"></i><span>Cities</span></a>
</li>

<li class="{{ Request::is('countries*') ? 'active' : '' }}">
    <a href="{!! route('countries.index') !!}"><i class="fa fa-edit"></i><span>Countries</span></a>
</li>

<li class="{{ Request::is('deliveryCharges*') ? 'active' : '' }}">
    <a href="{!! route('deliveryCharges.index') !!}"><i class="fa fa-edit"></i><span>Delivery Charges</span></a>
</li>

<li class="{{ Request::is('invoices*') ? 'active' : '' }}">
    <a href="{!! route('invoices.index') !!}"><i class="fa fa-edit"></i><span>Invoices</span></a>
</li>

<li class="{{ Request::is('shippings*') ? 'active' : '' }}">
    <a href="{!! route('shippings.index') !!}"><i class="fa fa-edit"></i><span>Shippings</span></a>
</li>

<li class="{{ Request::is('subCategories*') ? 'active' : '' }}">
    <a href="{!! route('subCategories.index') !!}"><i class="fa fa-edit"></i><span>Sub Categories</span></a>
</li>

<li class="{{ Request::is('trades*') ? 'active' : '' }}">
    <a href="{!! route('trades.index') !!}"><i class="fa fa-edit"></i><span>Trades</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('Cart*') ? 'active' : '' }}">
{{--    {{ dd(Session::get('cart', 'default')) }}--}}
    <a href="{!! route('products.cart') !!}"><i class="fa fa-edit"></i><span>Cart </span><span class="badge badge-light">{{ Session::has('cart')? Session::get('cart', 'default')->users[Auth::user()->id]["finalQuantity"]:"" }}</span></a>
</li>
