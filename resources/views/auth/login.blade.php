@extends('layouts.app_base')

@section('content')
    <!-- Page Content -->
    <div class="bg-body-dark bg-pattern" style="background-image: url('{{ asset('/media/various/bg-pattern-inverse.png') }}')">
        <div class="row mx-0 justify-content-center">
            <div class="hero-static col-lg-6 col-xl-4">
                <div class="content content-full overflow-hidden">
                    <!-- Header -->
                    <div class="py-30 text-center">
                        <a class="link-effect font-w700 mr-5" href="{{ route('main.index') }}">
                            {{--                        <i class="si si-home text-dual-primary"></i>--}}
                            <span class="font-size-xl text-danger">Connect</span><span
                                    class="font-size-xl text-dual-primary-dark">Indonesia</span>
                        </a>
                    </div>
                    <!-- END Header -->

                    <!-- Sign In Form -->
                    <!-- jQuery Validation functionality is initialized with .js-validation-signin class in js/pages/op_auth_signin.min.js which was auto compiled from _es6/pages/op_auth_signin.js -->
                    <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <form class="js-validation-signin" method="post" action="{{ url('/login') }}">
                        {!! csrf_field() !!}
                        <div class="block block-themed block-rounded block-shadow">
                            <div class="block-header bg-gd-dusk">
                                <h3 class="block-title">Please Sign In</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-wrench"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group row has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-12">
                                        <label for="login-username">Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                               id="login-username">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>
                                                    {{ $errors->first('email') }}
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-12">
                                        <label for="login-password">Password</label>
                                        <input type="password" class="form-control" id="login-password" name="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>
                                                    {{ $errors->first('password') }}
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-sm-6 d-sm-flex align-items-center push">
                                        <div class="custom-control custom-checkbox mr-auto ml-0 mb-0">
                                            <input type="checkbox" class="custom-control-input" id="login-remember-me"
                                                   name="remember">
                                            <label class="custom-control-label" for="login-remember-me">Remember
                                                Me</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-sm-right push">
                                        <button type="submit" class="btn btn-alt-primary">
                                            <i class="si si-login mr-10"></i> Sign In
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content bg-body-light">
                                <div class="form-group text-center">
                                    <a class="link-effect text-muted mr-10 mb-5 d-inline-block"
                                       href="{{ url('/register') }}">
                                        <i class="fa fa-plus mr-5"></i> Create Account
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END Sign In Form -->
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <title>InfyOm Laravel Generator</title>--}}

{{--    <!-- Tell the browser to be responsive to screen width -->--}}
{{--    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">--}}

{{--    <!-- Bootstrap 3.3.7 -->--}}
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

{{--    <!-- Font Awesome -->--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}

{{--    <!-- Ionicons -->--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">--}}

{{--    <!-- Theme style -->--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">--}}

{{--    <!-- iCheck -->--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">--}}

{{--    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
{{--    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
{{--    <!--[if lt IE 9]>--}}
{{--    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
{{--    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
{{--    <![endif]-->--}}

{{--</head>--}}
{{--<body class="hold-transition login-page">--}}
{{--<div class="login-box">--}}
{{--    <div class="login-logo">--}}
{{--        <a href="{{ url('/') }}"><b>InfyOm </b>Generator</a>--}}
{{--    </div>--}}

{{--    <!-- /.login-logo -->--}}
{{--    <div class="login-box-body">--}}
{{--        <p class="login-box-msg">Sign in to start your session</p>--}}

{{--        <form method="post" action="{{ url('/login') }}">--}}
{{--            {!! csrf_field() !!}--}}

{{--            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">--}}
{{--                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
{{--                @if ($errors->has('email'))--}}
{{--                    <span class="help-block">--}}
{{--                    <strong>{{ $errors->first('email') }}</strong>--}}
{{--                </span>--}}
{{--                @endif--}}
{{--            </div>--}}

{{--            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--                <input type="password" class="form-control" placeholder="Password" name="password">--}}
{{--                <span class="glyphicon glyphicon-lock form-control-feedback"></span>--}}
{{--                @if ($errors->has('password'))--}}
{{--                    <span class="help-block">--}}
{{--                    <strong>{{ $errors->first('password') }}</strong>--}}
{{--                </span>--}}
{{--                @endif--}}

{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-8">--}}
{{--                    <div class="checkbox icheck">--}}
{{--                        <label>--}}
{{--                            <input type="checkbox" name="remember"> Remember Me--}}
{{--                        </label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- /.col -->--}}
{{--                <div class="col-xs-4">--}}
{{--                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--}}
{{--                </div>--}}
{{--                <!-- /.col -->--}}
{{--            </div>--}}
{{--        </form>--}}

{{--        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>--}}
{{--        <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>--}}

{{--    </div>--}}
{{--    <!-- /.login-box-body -->--}}
{{--</div>--}}
{{--<!-- /.login-box -->--}}

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}

{{--<!-- AdminLTE App -->--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>--}}

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>--}}
{{--<script>--}}
{{--    $(function () {--}}
{{--        $('input').iCheck({--}}
{{--            checkboxClass: 'icheckbox_square-blue',--}}
{{--            radioClass: 'iradio_square-blue',--}}
{{--            increaseArea: '20%' // optional--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
