@extends('layouts.app_codebase')

@section('content')
    <section class="content-header">
        <h1>
            Companies
        </h1>
    </section>
    <div class="block">
        <div class="block-header block-header-default">
            <h1 class="block-title">
                User
            </h1>
        </div>
        <div class="block-content">
            <div class="row" style="padding-left: 20px">
                @include('invoices.show_fields')
            </div>
            <p>
                <a href="{!! url()->previous() !!}" class="btn btn-primary">Back</a>
            </p>
        </div>
    </div>
@endsection
