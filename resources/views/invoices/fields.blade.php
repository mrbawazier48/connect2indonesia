<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Buyer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Buyer', 'Buyer:') !!}
    {!! Form::select('Buyer', $companies , null , ['class' => 'form-control']) !!}
{{--    {!! Form::number('Buyer', null, ['class' => 'form-control']) !!}--}}
</div>

<!-- Seller Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Seller', 'Seller:') !!}
    {!! Form::select('Seller', $companies , null , ['class' => 'form-control']) !!}
{{--    {!! Form::number('Seller', null, ['class' => 'form-control']) !!}--}}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('invoices.index') !!}" class="btn btn-default">Cancel</a>
</div>
