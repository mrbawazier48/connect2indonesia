<div class="table-responsive">
    <table class="table table-vcenter">
        <tbody>
        <tr>
            <td style="width: 8%;">Date</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $invoices->date !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Buyer</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $invoices->buyer->name !!}</td>
        </tr>
        <tr>
            <td style="width: 8%;">Seller</td>
            <td style="width: 2%;"> : </td>
            <td>{!! $invoices->seller->name !!}</td>
        </tr>
        </tbody>
    </table>
</div>

{{--<!-- Date Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('date', 'Date:') !!}--}}
{{--    <p>{!! $invoices->date !!}</p>--}}
{{--</div>--}}

{{--<!-- Buyer Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('Buyer', 'Buyer:') !!}--}}
{{--    <p>{!! $invoices->Buyer !!}</p>--}}
{{--</div>--}}

{{--<!-- Seller Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('Seller', 'Seller:') !!}--}}
{{--    <p>{!! $invoices->Seller !!}</p>--}}
{{--</div>--}}

