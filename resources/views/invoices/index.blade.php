@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1 class="block-title">Companies</h1>
            <div class="block-option">
                <button type="button" class="btn btn-secondary"><a href="{!! route('invoices.create') !!}">Add New</a>
                </button>
            </div>
        </div>
        <div class="block-content">
            <p>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('invoices.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

