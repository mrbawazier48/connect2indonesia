<div class="table-responsive">
    <table class="table" id="invoices-table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Product</th>
            <th>Shipping</th>
            <th>Delivery Target</th>
            <th>Total</th>
            <th>Buyer</th>
            <th>Seller</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoices)
            <tr>
                <td>{!! $invoices->date !!}</td>
                <td>
                    @foreach ($invoices->products as $product)
                        {{ $product->name }}
                    @endforeach
                </td>
                <td>
                    @foreach ($invoices->shippings as $shipping)
                        {{ $shipping->name }}
                    @endforeach
                </td>
                <td>
                    @foreach ($invoices->deliveryCharges as $delivery)
                        {{ $delivery->name }}
                    @endforeach
                </td>
                <td>{{ $invoices->total }}</td>
                <td>
                    @foreach($invoices->Buyer as $buyer)
                        {!! $buyer->name !!}
                    @endforeach
                </td>
                <td>
                    @foreach ($invoices->Seller as $seller)
                        {{ $seller->name }}
                    @endforeach
                </td>
                <td>
                    {!! Form::open(['route' => ['invoices.destroy', $invoices->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('invoices.show', [$invoices->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('invoices.edit', [$invoices->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
