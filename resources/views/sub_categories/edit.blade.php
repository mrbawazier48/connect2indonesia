@extends('layouts.app_codebase')

@section('content')
    <div class="block p-5">
        <div class="block-header block-header-default">
            <h1>
                Sub Categories
            </h1>
        </div>
        <div class="block-content">
            <p>
       @include('adminlte-templates::common.errors')
            </p>
                   {!! Form::model($subCategories, ['route' => ['subCategories.update', $subCategories->id], 'method' => 'patch']) !!}

                        @include('sub_categories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
@endsection
