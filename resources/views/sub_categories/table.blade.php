<div class="table-responsive">
    <table class="table" id="subCategories-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Categories Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($subCategories as $subCategories)
            <tr>
                <td>{!! $subCategories->name !!}</td>
            <td>{!! $subCategories->Categories_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['subCategories.destroy', $subCategories->id], 'method' => 'delete']) !!}
                    <div class='col-auto btn-group'>
                        <a href="{!! route('subCategories.show', [$subCategories->id]) !!}" class='btn btn-info btn-xs'
                           title="Detail"><i class="si si-eye"></i></a>
                        <a href="{!! route('subCategories.edit', [$subCategories->id]) !!}" class='btn btn-success btn-xs'
                           title="Edit"><i class="si si-pencil"></i></a>
                        {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')",'title' => 'Delete']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
