<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @package App\Models
 * @version November 29, 2019, 7:27 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection companies
 * @property \Illuminate\Database\Eloquent\Collection deliveryCharges
 * @property \Illuminate\Database\Eloquent\Collection products
 * @property \Illuminate\Database\Eloquent\Collection company1s
 * @property \Illuminate\Database\Eloquent\Collection shippings
 * @property string date
 * @property integer total
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'date',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'date' => 'date',
        'total' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function buyer()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'buyer','Invoice_Items_id','Companies_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function deliveryCharges()
    {
        return $this->belongsToMany(\App\Models\DeliveryCharge::class, 'delivery_charge_has_invoice_items','Invoice_Items_id','Delivery_Charge_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'products_has_invoice_items','Invoice_Items_id','Products_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function seller()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'seller','Invoices_id','Companies_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function shippings()
    {
        return $this->belongsToMany(\App\Models\Shipping::class, 'shippings_has_invoice_items','Invoice_Items_id','Shippings_id');
    }
}
