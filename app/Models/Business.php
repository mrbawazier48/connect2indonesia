<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Business
 * @package App\Models
 * @version November 7, 2019, 3:43 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection companies
 * @property string name
 */
class Business extends Model
{
    use SoftDeletes;

    public $table = 'businesses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function companies()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'companies_has_businesses');
    }
}
