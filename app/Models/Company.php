<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 * @package App\Models
 * @version November 29, 2019, 10:28 pm UTC
 *
 * @property \App\Models\City cities
 * @property \Illuminate\Database\Eloquent\Collection invoices
 * @property \Illuminate\Database\Eloquent\Collection businesses
 * @property \Illuminate\Database\Eloquent\Collection products
 * @property \Illuminate\Database\Eloquent\Collection invoice1s
 * @property string name
 * @property string description
 * @property string address
 * @property string web
 * @property integer telephone
 * @property string avatar
 * @property integer Cities_id
 */
class Company extends Model
{
    use SoftDeletes;

    public $table = 'companies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'address',
        'web',
        'telephone',
        'avatar',
        'Cities_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'address' => 'string',
        'web' => 'string',
        'telephone' => 'integer',
        'avatar' => 'string',
        'Cities_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Cities_id' => 'required',
        'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cities()
    {
        return $this->belongsTo(\App\Models\City::class, 'Cities_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function invoices()
    {
        return $this->belongsToMany(\App\Models\Invoice::class, 'buyer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function businesses()
    {
        return $this->belongsToMany(\App\Models\Business::class, 'companies_has_businesses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\App\Models\Product::class, 'Companies_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function invoice1s()
    {
        return $this->belongsToMany(\App\Models\Invoice::class, 'seller');
    }

    public function user(){
        return $this->belongsTo(\App\Models\User::class);
    }
}
