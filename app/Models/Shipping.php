<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Shipping
 * @package App\Models
 * @version November 29, 2019, 7:25 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection invoices
 * @property string name
 * @property integer price
 */
class Shipping extends Model
{
    use SoftDeletes;

    public $table = 'shippings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'price' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function invoices()
    {
        return $this->belongsToMany(\App\Models\Invoice::class, 'shippings_has_invoice_items','Invoices_id','Shippings_id');
    }
}
