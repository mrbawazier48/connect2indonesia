<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version November 29, 2019, 7:26 pm UTC
 *
 * @property \App\Models\Category categories
 * @property \App\Models\Company companies
 * @property \App\Models\Trade trades
 * @property \Illuminate\Database\Eloquent\Collection invoices
 * @property string name
 * @property string description
 * @property string avatar
 * @property integer price
 * @property integer Categories_id
 * @property integer Trades_id
 * @property integer Companies_id
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'avatar',
        'price',
        'Categories_id',
        'Trades_id',
        'Companies_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'avatar' => 'string',
        'price' => 'integer',
        'Categories_id' => 'integer',
        'Trades_id' => 'integer',
        'Companies_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Categories_id' => 'required',
        'Trades_id' => 'required',
        'Companies_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categories()
    {
        return $this->belongsTo(\App\Models\Category::class, 'Categories_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function companies()
    {
        return $this->belongsTo(\App\Models\Company::class, 'Companies_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function trades()
    {
        return $this->belongsTo(\App\Models\Trade::class, 'Trades_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function invoices()
    {
        return $this->belongsToMany(\App\Models\Invoice::class, 'products_has_invoice_items','Invoice_Items_id','Products_id');
    }
}
