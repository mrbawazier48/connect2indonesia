<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;


class CartChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('cart')){
            return $next($request);
        }else{
            return redirect(route("main.index"));
        }

    }
}
