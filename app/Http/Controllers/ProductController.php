<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Company;
use App\Models\Company as Companies;
use App\Models\Product;
use App\Models\Trade;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;
use Session;
use App\Cart;
use App\Models\Category;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    public function getFile($request){
        $input = $request->all();
        if ($request->hasFile('avatar')){
            $file = $input['avatar'];
            $extension = $file->getClientOriginalExtension();
            $filename =  time().'.'.$extension;
            $file->move('uploads/product',$filename);
            return $filename;
        }else{
            return '';
        }
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = $this->productRepository->all();
        $categories = Category::all()->pluck('name','id');


        return view('products.index')
            ->with('products', $products)
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name','id');
        $trades = Trade::all()->pluck('name','id');
        $companies = Companies::all()->pluck('name','id');
//        dd($companies);

        return view('products.create')
            ->with('categories', $categories)
            ->with('trades', $trades)
            ->with('companies', $companies);

    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)

    {
        $input = $request->all();

        $product = $this->productRepository->create($input);
        $product->avatar = $this->getFile($request);

        $product->save();

        Flash::success('Product saved successfully.');

        if (Auth::user()->id==1) {
            return redirect(route('products.index'));
        }else{
            return redirect(route("products.showProductCompany",[$request->Companies_id]));
        }
    }
    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = Category::all()->pluck('name','id');
        $trades = Trade::all()->pluck('name','id');
        $companies = Companies::all()->pluck('name','id');
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')
            ->with('product', $product)
            ->with('categories', $categories)
            ->with('trades', $trades)
            ->with('companies', $companies);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $input = $request->all();
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product = $this->productRepository->update($request->all(), $id);
        $product->avatar = $this->getFile($request);
        $product->save();

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
    public function addToCart(Request $request ,$id){
        $product = Product::find($id);
        $company_id = $product->Companies_id;
        $company = Company::find($company_id);
        $user = Auth::user();
        $oldCart = Session::has('cart')? Session::get('cart', 'default'): null;
        $cart = new Cart($oldCart);
//        dd($cart);
        $cart->add($user,$user->id,$company,$company_id,$product, $product->id);
//        dd($cart);
        $request->session()->put('cart',$cart);
        return back();
    }

    public function getCart(){
        if (!Session::has('cart')){
            return view('products.cart');
        }
        $user_id = Auth::user()->id;
        if (Session::get('cart', 'default')->users==null){
            Session::forget('cart');
        }
        $oldCart = Session::has('cart')? Session::get('cart', 'default'): null;
        $cart = new Cart($oldCart);
        return view('products.cart',["companies"=>$cart->users[$user_id]["companies"]]);
    }

    public function increaseItemInCart(Request $request, $product_id){
        $user_id = Auth::user()->id;
        $product = Product::find($product_id);
        $company_id = $product->Companies_id;
        $oldCart = Session::get('cart', 'default');
        $cart = new Cart($oldCart);
        $cart->increase($user_id,$company_id,$product);
        $request->session()->put('cart',$cart);
        return back();
    }

    public function decreaseItemInCart(Request $request,$product_id){
        $user_id = Auth::user()->id;
        $product = Product::find($product_id);
        $company_id = $product->Companies_id;
        $oldCart = Session::get('cart', 'default');
        $cart = new Cart($oldCart);
        $cart->decrease($user_id,$company_id,$product);
        $request->session()->put('cart',$cart);
        return back();
    }

    public function showProductCompany($company_id){
        $products = Product::where("Companies_id", $company_id)->get();
        return view('products.index')
            ->with("products",$products)
            ->with("company_id",$company_id);

    }

    public function createProductCompany($company_id){
        $categories = Category::all()->pluck('name','id');
        $trades = Trade::all()->pluck('name','id');
        $companies = Companies::all()->pluck('name','id');
//        dd($company_id);

        return view('products.create')
            ->with('categories', $categories)
            ->with('trades', $trades)
            ->with('companies', $companies)
            ->with("company_id",$company_id);

    }
    public function editProductCompany($company_id,$product_id){
        $categories = Category::all()->pluck('name','id');
        $trades = Trade::all()->pluck('name','id');
        $companies = Companies::all()->pluck('name','id');
        $product = $this->productRepository->find($product_id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')
            ->with('product', $product)
            ->with('categories', $categories)
            ->with('trades', $trades)
            ->with('companies', $companies)
            ->with('company_id',$company_id);
    }
}
