<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Http\Requests\CreateInvoicesRequest;
use App\Http\Requests\UpdateInvoicesRequest;
use App\Models\Company;
use App\Models\DeliveryCharge;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Shipping;
use App\Models\User;
use App\Repositories\InvoicesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Response;

class InvoicesController extends AppBaseController
{
    /** @var  InvoicesRepository */
    private $invoicesRepository;

    public function __construct(InvoicesRepository $invoicesRepo)
    {
        $this->invoicesRepository = $invoicesRepo;
    }

    /**
     * Display a listing of the Invoices.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $invoices = $this->invoicesRepository->all();

        return view('invoices.index')
            ->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new Invoices.
     *
     * @return Response
     */
    public function create()
    {
        $companies = Company::all()->pluck("name","id");

        return view('invoices.create')
            ->with("companies", $companies);
    }

    /**
     * Store a newly created Invoices in storage.
     *
     * @param CreateInvoicesRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoicesRequest $request)
    {
        $input = $request->all();

        $now = \Carbon\Carbon::now();
//        $now = $now->format('Y-m-d');

//        $invoices = $this->invoicesRepository->create($input);
        $invoices = new Invoice();
//        dd($invoices);
        $invoices->total = $input["total"];
        $invoices->date = $now;
        $invoices->save();
//        dd($input["Seller"]);
        $invoices->seller()->attach($input["Seller"]);
        $invoices->buyer()->attach($input["Buyer"]);
        $invoices->products()->attach($input["product"]);
        $invoices->deliveryCharges()->attach($input["delivery"]);
        $invoices->shippings()->attach($input["shipping"]);

        $this->clearProductCart($input["product"]);

        Flash::success('Invoices saved successfully.');

        return redirect(route('invoices.index'));
    }

    /**
     * Display the specified Invoices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $invoices = $this->invoicesRepository->find($id);

        if (empty($invoices)) {
            Flash::error('Invoices not found');

            return redirect(route('invoices.index'));
        }

        return view('invoices.show')->with('invoices', $invoices);
    }

    /**
     * Show the form for editing the specified Invoices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $invoices = $this->invoicesRepository->find($id);

        if (empty($invoices)) {
            Flash::error('Invoices not found');

            return redirect(route('invoices.index'));
        }

        return view('invoices.edit')->with('invoices', $invoices);
    }

    /**
     * Update the specified Invoices in storage.
     *
     * @param int $id
     * @param UpdateInvoicesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoicesRequest $request)
    {
        $invoices = $this->invoicesRepository->find($id);

        if (empty($invoices)) {
            Flash::error('Invoices not found');

            return redirect(route('invoices.index'));
        }

        $invoices = $this->invoicesRepository->update($request->all(), $id);

        Flash::success('Invoices updated successfully.');

        return redirect(route('invoices.index'));
    }

    /**
     * Remove the specified Invoices from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $invoices = $this->invoicesRepository->find($id);

        if (empty($invoices)) {
            Flash::error('Invoices not found');

            return redirect(route('invoices.index'));
        }

        $this->invoicesRepository->delete($id);

        Flash::success('Invoices deleted successfully.');

        return redirect(route('invoices.index'));
    }

    public function checkout ($product_id){
        $cart   = Session::get('cart', 'default');
        $product = Product::find($product_id);
//        dd($product);
        $company_id = $product->Companies_id;
//        dd($company_id);
//        dd($cart->users[Auth::user()->id]["companies"]);
        $productCart = $cart->users[Auth::user()->id]["companies"][$company_id];
//        dd($productCart);
        $shippings = Shipping::all()->pluck("name","id");
        $shippings->prepend("Select One","");
        $shippingPrices = Shipping::all()->pluck("price","id");
        $shippingPrices->prepend("-","");
        $deliveries = DeliveryCharge::all()->pluck("name","id");
        $deliveries->prepend("Select One","");
        $deliveryPrices = DeliveryCharge::all()->pluck("price","id");
        $deliveryPrices->prepend("-","");
        return view("main.checkout")
            ->with("shippings",$shippings)
            ->with("deliveries",$deliveries)
            ->with("product",$product)
            ->with("cartItem",$productCart)
            ->with("deliveryPrices",$deliveryPrices)
            ->with("shippingPrices",$shippingPrices);
    }

    public function clearProductCart($product_id){
        $user_id = Auth::user()->id;
        $product = Product::find($product_id);
        $company_id = $product->Companies_id;
        $oldCart = Session::get('cart', 'default');
        $cart = new Cart($oldCart);
        $cart->checkout($user_id,$company_id,$product);
        session()->put('cart',$cart);
    }
}
