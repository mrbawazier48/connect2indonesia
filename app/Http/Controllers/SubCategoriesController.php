<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubCategoriesRequest;
use App\Http\Requests\UpdateSubCategoriesRequest;
use App\Repositories\SubCategoriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SubCategoriesController extends AppBaseController
{
    /** @var  SubCategoriesRepository */
    private $subCategoriesRepository;

    public function __construct(SubCategoriesRepository $subCategoriesRepo)
    {
        $this->subCategoriesRepository = $subCategoriesRepo;
    }

    /**
     * Display a listing of the SubCategories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $subCategories = $this->subCategoriesRepository->all();

        return view('sub_categories.index')
            ->with('subCategories', $subCategories);
    }

    /**
     * Show the form for creating a new SubCategories.
     *
     * @return Response
     */
    public function create()
    {
        return view('sub_categories.create');
    }

    /**
     * Store a newly created SubCategories in storage.
     *
     * @param CreateSubCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateSubCategoriesRequest $request)
    {
        $input = $request->all();

        $subCategories = $this->subCategoriesRepository->create($input);

        Flash::success('Sub Categories saved successfully.');

        return redirect(route('subCategories.index'));
    }

    /**
     * Display the specified SubCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subCategories = $this->subCategoriesRepository->find($id);

        if (empty($subCategories)) {
            Flash::error('Sub Categories not found');

            return redirect(route('subCategories.index'));
        }

        return view('sub_categories.show')->with('subCategories', $subCategories);
    }

    /**
     * Show the form for editing the specified SubCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subCategories = $this->subCategoriesRepository->find($id);

        if (empty($subCategories)) {
            Flash::error('Sub Categories not found');

            return redirect(route('subCategories.index'));
        }

        return view('sub_categories.edit')->with('subCategories', $subCategories);
    }

    /**
     * Update the specified SubCategories in storage.
     *
     * @param int $id
     * @param UpdateSubCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubCategoriesRequest $request)
    {
        $subCategories = $this->subCategoriesRepository->find($id);

        if (empty($subCategories)) {
            Flash::error('Sub Categories not found');

            return redirect(route('subCategories.index'));
        }

        $subCategories = $this->subCategoriesRepository->update($request->all(), $id);

        Flash::success('Sub Categories updated successfully.');

        return redirect(route('subCategories.index'));
    }

    /**
     * Remove the specified SubCategories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subCategories = $this->subCategoriesRepository->find($id);

        if (empty($subCategories)) {
            Flash::error('Sub Categories not found');

            return redirect(route('subCategories.index'));
        }

        $this->subCategoriesRepository->delete($id);

        Flash::success('Sub Categories deleted successfully.');

        return redirect(route('subCategories.index'));
    }
}
