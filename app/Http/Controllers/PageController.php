<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){

        $products = Product::all();
//        dd($products);
        return view('main.index')
            ->with('products', $products);
    }

    public function detail($product_name){
        $product_name = urldecode($product_name);
        $product = Product::where("name",$product_name)->first();

        return view("main.description")
            ->with("product",$product);
    }
}
