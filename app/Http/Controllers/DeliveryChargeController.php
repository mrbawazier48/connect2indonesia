<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryChargeRequest;
use App\Http\Requests\UpdateDeliveryChargeRequest;
use App\Repositories\DeliveryChargeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryChargeController extends AppBaseController
{
    /** @var  DeliveryChargeRepository */
    private $deliveryChargeRepository;

    public function __construct(DeliveryChargeRepository $deliveryChargeRepo)
    {
        $this->deliveryChargeRepository = $deliveryChargeRepo;
    }

    /**
     * Display a listing of the DeliveryCharge.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryCharges = $this->deliveryChargeRepository->all();

        return view('delivery_charges.index')
            ->with('deliveryCharges', $deliveryCharges);
    }

    /**
     * Show the form for creating a new DeliveryCharge.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_charges.create');
    }

    /**
     * Store a newly created DeliveryCharge in storage.
     *
     * @param CreateDeliveryChargeRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryChargeRequest $request)
    {
        $input = $request->all();

        $deliveryCharge = $this->deliveryChargeRepository->create($input);

        Flash::success('Delivery Charge saved successfully.');

        return redirect(route('deliveryCharges.index'));
    }

    /**
     * Display the specified DeliveryCharge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryCharge = $this->deliveryChargeRepository->find($id);

        if (empty($deliveryCharge)) {
            Flash::error('Delivery Charge not found');

            return redirect(route('deliveryCharges.index'));
        }

        return view('delivery_charges.show')->with('deliveryCharge', $deliveryCharge);
    }

    /**
     * Show the form for editing the specified DeliveryCharge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryCharge = $this->deliveryChargeRepository->find($id);

        if (empty($deliveryCharge)) {
            Flash::error('Delivery Charge not found');

            return redirect(route('deliveryCharges.index'));
        }

        return view('delivery_charges.edit')->with('deliveryCharge', $deliveryCharge);
    }

    /**
     * Update the specified DeliveryCharge in storage.
     *
     * @param int $id
     * @param UpdateDeliveryChargeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryChargeRequest $request)
    {
        $deliveryCharge = $this->deliveryChargeRepository->find($id);

        if (empty($deliveryCharge)) {
            Flash::error('Delivery Charge not found');

            return redirect(route('deliveryCharges.index'));
        }

        $deliveryCharge = $this->deliveryChargeRepository->update($request->all(), $id);

        Flash::success('Delivery Charge updated successfully.');

        return redirect(route('deliveryCharges.index'));
    }

    /**
     * Remove the specified DeliveryCharge from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryCharge = $this->deliveryChargeRepository->find($id);

        if (empty($deliveryCharge)) {
            Flash::error('Delivery Charge not found');

            return redirect(route('deliveryCharges.index'));
        }

        $this->deliveryChargeRepository->delete($id);

        Flash::success('Delivery Charge deleted successfully.');

        return redirect(route('deliveryCharges.index'));
    }
}
