<?php

namespace App\Repositories;

use App\Models\Shipping;
use App\Repositories\BaseRepository;

/**
 * Class ShippingRepository
 * @package App\Repositories
 * @version November 29, 2019, 7:25 pm UTC
*/

class ShippingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shipping::class;
    }
}
