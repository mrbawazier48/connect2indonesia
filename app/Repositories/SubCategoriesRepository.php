<?php

namespace App\Repositories;

use App\Models\SubCategories;
use App\Repositories\BaseRepository;

/**
 * Class SubCategoriesRepository
 * @package App\Repositories
 * @version November 7, 2019, 3:50 pm UTC
*/

class SubCategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'Categories_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubCategories::class;
    }
}
