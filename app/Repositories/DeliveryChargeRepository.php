<?php

namespace App\Repositories;

use App\Models\DeliveryCharge;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryChargeRepository
 * @package App\Repositories
 * @version November 29, 2019, 7:24 pm UTC
*/

class DeliveryChargeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryCharge::class;
    }
}
