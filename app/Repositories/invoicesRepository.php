<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Models\Invoices;
use App\Repositories\BaseRepository;

/**
 * Class InvoicesRepository
 * @package App\Repositories
 * @version November 28, 2019, 6:28 pm UTC
*/

class InvoicesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'Buyer',
        'Seller'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invoice::class;
    }
}
