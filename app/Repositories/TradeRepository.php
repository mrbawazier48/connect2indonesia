<?php

namespace App\Repositories;

use App\Models\Trade;
use App\Repositories\BaseRepository;

/**
 * Class TradeRepository
 * @package App\Repositories
 * @version November 7, 2019, 3:51 pm UTC
*/

class TradeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trade::class;
    }
}
