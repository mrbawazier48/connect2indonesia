<?php

namespace App;

class Cart
{
    public $users;
//    public $companies;
//    public $products;
//    public $items;
//    public $companies;
//    public $totalQty = 0;
//    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->users = $oldCart->users;
//            $this->items = $oldCart->items;
//            $this->totalQty = $oldCart->totalQty;
//            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($user, $user_id, $company, $company_id, $product, $product_id)
    {

        $totalPrice = 0;
        $totalQuantity = 0;
        $finalQuantity = 0;

        //Struktur Array $storeUser
        $storeProduct = ['product' => $product, "subTotalQuantity" => 0, 'subTotalPrice' => $product->price];
        $productList = [$product_id => $storeProduct];
        $storeCompany = ["company" => $company, 'totalQuantity' => 0, "totalPrice" => $product->Companies_id == $company_id ? $product->price : 0, "products" => $productList];
        $companyList = [$company_id => $storeCompany];
        $storeUser = ['user' => $user, "finalQuantity" => 0, "companies" => $companyList];

        //Jika Data User Sudah ada
        //Maka Data User akan dipakai di dalam variable $storedUser
        if ($this->users) {
//            dd(array_key_exists($company_id,$this->users[$user->id]["companies"]));
            if (array_key_exists($company_id, $this->users[$user->id]["companies"])) {
                $storeUser = $this->users[$user_id];
                $storeUser["companies"][$company_id] = $this->users[$user_id]["companies"][$company_id];
                if ($this->users[$user->id]["companies"]) {
                    //Data sama persis
//            dd($this->users[$user->id]["companies"]);
//                    dd(array_key_exists($product->id, $this->users[$user->id]["companies"]));
                    if (array_key_exists($product->id, $this->users[$user->id]["companies"][$company->id]["products"])) {
                        $storeUser = $this->users[$user->id];
                        $storeUser["companies"][$company->id] = $this->users[$user->id]["companies"][$company->id];
                        $storeUser["companies"][$company->id]["products"][$product->id] = $this->users[$user->id]["companies"]["$company->id"]["products"][$product->id];
                    } else {
                        $storeUser = $this->users[$user->id];
                        $storeUser["companies"][$company->id] = $this->users[$user->id]["companies"][$company->id];
                        $storeUser["companies"][$company->id]["products"][$product->id] = $storeProduct;
                    }
                }
            } else {
                $storeUser = $this->users[$user_id];
                $storeUser["companies"][$company_id] = $storeCompany;
//                dd($storeUser);
            }
        }


        //Shorthand :3
        $subTotalQuantity = $storeUser["companies"][$company_id]['products'][$product_id]["subTotalQuantity"];
        $productPrice = $storeUser["companies"][$company_id]['products'][$product_id]["product"]->price;

        //Formula
        $subTotalQuantity++;
        $subTotalPrice = $subTotalQuantity * $productPrice;

        //Masukin subTotalQuantity
        $storeUser["companies"][$company_id]['products'][$product_id]["subTotalQuantity"] = $subTotalQuantity;

        //Masukin subTotalPrice
        $storeUser["companies"][$company_id]['products'][$product_id]["subTotalPrice"] = $subTotalPrice;

        //Melakukan penghitungan totalPrice pada setiap product di dalam company


        //Melakukan penghitungan totalQuantity pada setiap product di dalam company
        foreach ($storeUser["companies"][$company_id]["products"] as $product) {
            $totalQuantity += $product["subTotalQuantity"];
        }

        $storeUser["companies"][$company_id]['totalQuantity'] = $totalQuantity;

        foreach ($storeUser["companies"][$company_id]["products"] as $product) {
            $totalPrice += $product["subTotalPrice"];
        }

        foreach ($storeUser["companies"] as $company) {
            $finalQuantity += $company["totalQuantity"];
        }

        $storeUser["finalQuantity"] = $finalQuantity;
        $storeUser["companies"][$company_id]['totalPrice'] = $totalPrice;

        $this->users[$user_id] = $storeUser;
    }

    public function decrease($user_id, $company_id, $product)
    {
        //Decreasing Quantity
        $this->users[$user_id]["finalQuantity"]--;
        $this->users[$user_id]["companies"][$company_id]["totalQuantity"]--;
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"]--;

        //Decreasing Price
        $this->users[$user_id]["companies"][$company_id]["totalPrice"] -= $product->price;
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalPrice"] -= $product->price;

        $this->clear($user_id,$company_id,$product);
    }

    public function increase($user_id, $company_id, $product)
    {
        //Increasing Quantity
        $this->users[$user_id]["finalQuantity"]++;
        $this->users[$user_id]["companies"][$company_id]["totalQuantity"]++;
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"]++;

        //Increasing Price
        $this->users[$user_id]["companies"][$company_id]["totalPrice"] += $product->price;
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalPrice"] += $product->price;
    }

    public function checkout($user_id, $company_id, $product){
        //Decreasing Quantity
        $this->users[$user_id]["finalQuantity"]-=$this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"];
        $this->users[$user_id]["companies"][$company_id]["totalQuantity"]-=$this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"];
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"]-=$this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"];

        //Decreasing Price
        $this->users[$user_id]["companies"][$company_id]["totalPrice"] -= $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalPrice"];
        $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalPrice"] -= $this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalPrice"];

        $this->clear($user_id,$company_id,$product);
    }

    public function clear($user_id,$company_id,$product){
        if ($this->users[$user_id]["companies"][$company_id]["products"][$product->id]["subTotalQuantity"] <= 0) {
            unset($this->users[$user_id]["companies"][$company_id]["products"][$product->id]);
        }
        if ($this->users[$user_id]["companies"][$company_id]["totalQuantity"] <= 0) {
            unset($this->users[$user_id]["companies"][$company_id]);
        }
        if ($this->users[$user_id]["finalQuantity"] <= 0) {
            $this->users = null;
        }
    }
}
