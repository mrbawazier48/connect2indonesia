<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');

//Main Page
Route::get('/', 'PageController@home')->name('main.index');
Route::get("product/{name}/detail", "PageController@detail")->name("main.detail");

//Route Group Auth
Route::group(['middleware' => 'auth'], function () {

    //All Model Resource
    Route::resource('businesses', 'BusinessController');
    Route::resource('categories', 'CategoryController');
    Route::resource('countries', 'CountryController');
    Route::resource('invoices', 'InvoicesController');
    Route::resource('subCategories', 'SubCategoriesController');
    Route::resource('trades', 'TradeController');
    Route::resource('users', 'UserController');
    Route::resource('deliveryCharges', 'DeliveryChargeController');
    Route::resource('shippings', 'ShippingController');
    Route::resource('products', 'ProductController');
    Route::resource('companies', 'CompanyController');
    Route::resource('cities', 'CityController');

    //Product Company
    Route::get('/company/{company_id}/products', 'ProductController@showProductCompany')->name("products.showProductCompany");
    Route::get('/company/{company_id}/product/create', "ProductController@createProductCompany")->name("products.createProductCompany");
    Route::get('/company/{company_id}/product/{product_id}', 'ProductController@editProductCompany')->name("products.editProductCompany");

    //Cart
    Route::get('/products/addToCart/{id}', 'ProductController@addToCart')->name('products.addToCart');
    Route::get('/cart/', "ProductController@getCart")->name('products.cart');
    Route::get('/cart/increase/{id}', "ProductController@increaseItemInCart")->name('increase.item');
    Route::get('/cart/decrease/{id}', "ProductController@decreaseItemInCart")->name('decrease.item');

    Route::group(['middleware' => 'CartChecker'], function () {

        //Checkout
        //Route::get('/checkout', "InvoicesController@Checkout")->name("invoice.checkout");
        Route::get('/checkout/{product_id}', "InvoicesController@checkout")->name("main.checkout");

    });
});



